var gulp = require('gulp');
var browserSync = require('browser-sync');
var injector = require('bs-html-injector');
var reload = browserSync.reload;
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var prettyHtml = require('gulp-pretty-html');
var plumber = require('gulp-plumber');
var postcss = require('gulp-postcss');
var lost = require('lost');
var watch = require('gulp-watch');


var minify = require('gulp-minify');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');
var gulpCopy = require('gulp-copy');
var csso = require('gulp-csso');
var rename = require('gulp-rename');
var critical = require('critical');
var babel = require('gulp-babel');
var rollup = require('gulp-rollup');

var path = {
  build: {
    html: './build/',
    js: './build/js/',
    css: './build/css/',
    files: './build/files/',
    images: './build/images/',
    fonts: './build/fonts/'
  },
  src: {
    html: './src/pug/pages/*.pug',
    js: './src/js/**/*.js',
    sass: './src/sass/**/*.scss',
    files: './src/files/**/*',
    images: './src/images/**/*',
    fonts: './src/fonts/**'
  },
  watch: {
    html: './src/pug/**/*.pug',
    js: './src/js/**/*.js',
    sass: './src/sass/**/*.scss',
    files: './src/files/**/*',
    images: './src/images/**/*',
    fonts: './src/fonts/**/*.*'
  },
  clean: './build'
};


gulp.task('browserSync', function () {
  browserSync.use(injector);
  browserSync({
    server: { baseDir: path.build.html },
    notify: false,
    open: false,
    ghostMode: false
  });
});

gulp.task('js', function () {
  return gulp.src([
    // './src/js/libs/axios.min.js',
    './node_modules/tiny-slider/dist/min/tiny-slider.js',
    './node_modules/smooth-scroll/dist/smooth-scroll.js',
    // './node_modules/choices.js/public/assets/scripts/choices.min.js',
    // './node_modules/svg.js/dist/svg.min.js',
    path.src.js
  ])
    .pipe(sourcemaps.init({ loadMap: true }))
    .pipe(plumber())
    .pipe(babel({
      presets: ['@babel/env'],
    }))
    .pipe(concat('main.js'))
    // .pipe(rollup({
    //   // any option supported by Rollup can be set here.
    //   input: './src/js/main.js',
    //   output: {
    //     format: 'es'
    //   }
    // }))
    // .pipe(minify({
    //   ext: {
    //     min: '.js'
    //   }
    // }))
    .pipe(uglify())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({ stream: true }));
});

gulp.task('sass', function () {
  gulp.src([path.src.sass])
    .pipe(sourcemaps.init({ loadMap: true }))
    .pipe(sass({ outputStyle: 'compressed' })).on('error', sass.logError)
    .pipe(postcss([
      lost()
      // autoprefixer()
    ]))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    // .pipe(concat('styles.css'))
    .pipe(csso())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest(path.build.css))
    .pipe(reload({ stream: true }));
});
gulp.task('pug', function buildHTML() {
  return gulp.src(path.src.html)
    .pipe(plumber())
    .pipe(pug())
    .pipe(prettyHtml({
      indent_size: 2,
      unformatted: ['']
    }))
    .pipe(gulp.dest(path.build.html))
    .pipe(reload({ stream: true }));
});
gulp.task('files', function () {
  return gulp.src([path.src.files, '!README.md'])
    .pipe(gulp.dest(path.build.files))
    .pipe(reload({ stream: true }));
});
gulp.task('images', function () {
  return gulp.src([path.src.images, '!README.md'])
    .pipe(gulp.dest(path.build.images))
    .pipe(reload({ stream: true }));
});
gulp.task('fonts', function () {
  return gulp.src([path.src.fonts, '!README.md'])
    .pipe(gulp.dest(path.build.fonts))
});
gulp.task('root-copy', function () {
  return gulp.src([
    './src/*.png',
    './src/browserconfig.xml',
    './src/favicon.ico',
    './src/manifest.json',
  ])
    .pipe(gulp.dest('./build/'))
});

gulp.task('critical', function () {
  critical.generate({
    inline: true,
    base: './build/',
    src: 'index.html',
    css: './build/css/styles.css',
    // extract: true,
    dest: 'index.html',
    minify: true,
    width: 1920,
    height: 930
  });
});


gulp.task('build', function () {
  runSequence('clean', 'sass', 'pug', 'files', 'images', 'fonts', 'js', 'root-copy');
});
gulp.task('clean', function () {
  return gulp.src('./build', { read: false })
    .pipe(clean());
});


gulp.task('watch', function () {
  watch(path.watch.sass, function () {
    setTimeout(function () {
      gulp.start('sass');
    }, 100);
  });
  watch(path.watch.html, function () {
    setTimeout(function () {
      gulp.start('pug');
    }, 100);
  });
  watch(path.watch.js, function () {
    setTimeout(function () {
      gulp.start('js');
    }, 100);
  });
  watch(path.watch.files, function () {
    setTimeout(function () {
      gulp.start('files');
    }, 100);
  });
  watch(path.watch.images, function () {
    setTimeout(function () {
      gulp.start('images');
    }, 100);
  });
  watch(path.watch.fonts, function () {
    setTimeout(function () {
      gulp.start('fonts');
    }, 100);
  });
});
gulp.task('default', ['build', 'browserSync', 'watch']);
