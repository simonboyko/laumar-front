(function (d) {
  /**
   * Табы на странице ВНЖ.
   */

  try {

    let tabs = d.querySelectorAll('.vnzTypesCard');
    function tabClickHandler(e) {
      let el = e.currentTarget;

      let currentActiveTab = d.querySelector('.vnzTypesCard--active');
      currentActiveTab.classList.remove('vnzTypesCard--active');

      let currentActiveContent = d.querySelectorAll('.vnzTabs__tab--active');
      currentActiveContent
        .forEach(el => el.classList.remove('vnzTabs__tab--active'));

      el.classList.add('vnzTypesCard--active');

      let nextTabId = el.getAttribute('href');
      let nextActiveEls = d.querySelectorAll(`${nextTabId}, ${nextTabId.replace('#', '.')}`);
      nextActiveEls
        .forEach(el => el.classList.add('vnzTabs__tab--active'));
    }
    tabs.forEach(tab => tab.addEventListener('click', tabClickHandler));

  } catch(e) {
    console.log(e);
  }
  

})(document);