/* Плавный скролл
 *
 * https://github.com/cferdinandi/smooth-scroll
 *
 ***/

(function () {
  if (dev) {
    console.log('INIT smooth-scroll');
  }

  new SmoothScroll('[data-scroll], .header__menu a[href*="#"]');

})();