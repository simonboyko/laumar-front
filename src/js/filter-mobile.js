/* Фильтр на мобильном телефоне на странице листинга */

(function () {
  if (!document.getElementsByClassName('filterListingBlock')[0]) return;

  if (dev) {
    console.log('INIT filter-mobile');
  }



  // блок с фильтром
  let filter = document.getElementsByClassName('filterListingBlock')[0];
  filter.opened = false;

  // кнопка фильтра в шапке
  let filterButton = document.getElementsByClassName('header__filter')[0];
  if (filterButton) {
    filterButton.addEventListener('click', filterButtonClickHandler);
  } else {
    console.error('Кнопка фильтра не найдена.');
  }
  function filterButtonClickHandler(e) {
    if (dev) {
      console.log('f: filterButtonClickHandler');
    }

    if (filter.opened) {
      closeFilter();
    } else {
      openFilter();
    }
  }


  // кнопка закрытия фильтра
  let closeFilterButton = document.getElementsByClassName('filterListingBlock__close')[0];
  if (closeFilterButton) {
    closeFilterButton.addEventListener('click', closeFilterButtonClickHandler);
  } else {
    console.error('Кнопка закрытия фильтра не найдена.');
  }
  function closeFilterButtonClickHandler(e) {
    if (dev) {
      console.log('f: closeFilterButtonClickHandler');
    }

    closeFilter();
  }


  // кнопка очистки фильтра
  let clearFilterButton = document.getElementsByClassName('filterListingBlock__clear')[0];
  if (clearFilterButton) {
    clearFilterButton.addEventListener('click', clearFilterButtonClickHandler);
  } else {
    console.error('Кнопка очтки фильтра не найдена.');
  }
  function clearFilterButtonClickHandler(e) {
    if (dev) {
      console.log('f: clearFilterButtonClickHandler');
    }

    clearFilter();
  }



  // открываем
  function openFilter() {
    if (dev) {
      console.log('f: openFilter');
    }

    filter.classList.add('filterListingBlock--opened');
    filter.opened = true;
    document.body.classList.add('noScroll');
  }

  // закрываем
  function closeFilter() {
    if (dev) {
      console.log('f: closeFilter');
    }

    filter.classList.remove('filterListingBlock--opened');
    filter.opened = false;
    document.body.classList.remove('noScroll');
  }
  
  // очистка фильтра
  function clearFilter() {
    if (dev) {
      console.log('f: clearFilter');
    }
  }

})();