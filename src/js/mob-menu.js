/* Скрипт для открывания меню-гамбургера. */

(function () {
  if (dev) {
    console.log('INIT mob-menu');
  }

  // проверяем, есть ли само меню и кнопка, если нет, выходим
  let button = document.querySelector('.mobMenu__button');
  let menu = document.querySelector('.mobMenu');
  if (!button && !menu) {
    console.error('Не найдены кнопка .mobMenu__button и мобильное меню .mobMenu.');
    return;
  } else if (!button && menu) {
    console.error('Не найдена кнопка .mobMenu__button.');
    return;
  } else if (button && !menu) {
    console.error('Не найдено мобильное меню .mobMenu.');
    return;
  }


  // **********************************
  // Открытие/закрытие мобильного меню
  // **********************************

  let opened = false;

  // вешаем клик на кнопку мобильного меню
  button.addEventListener('click', clickHandler);


  // вешаем клик на кнопку закрытия мобильного меню
  let closer = document.getElementsByClassName('mobMenu__close')[0];
  // если закрывашки нет, выходим из скрипта
  if (!closer) {
    console.error('Нет кнопки закрытия мобильного меню.');
    return;
  }
  closer.addEventListener('click', clickHandler);


  // вызываем, когда надо открыть меню
  function openMenu() {
    if (dev) {
      console.log('f: openMenu');
    }

    opened = true;
    button.classList.add('mobMenu__button--opened');
    menu.classList.add('mobMenu--opened');
    document.body.classList.add('noScroll');
  }

  // вызываем, когда надо закрыть меню
  function closeMenu() {
    if (dev) {
      console.log('f: closeMenu');
    }

    opened = false;
    button.classList.remove('mobMenu__button--opened');
    menu.classList.remove('mobMenu--opened');
    document.body.classList.remove('noScroll');
  }

  // обработчик на клик для кнопки мобильного меню и закрытия
  function clickHandler(e) {
    if (dev) {
      console.log('f: clickHandler');
    }

    if (!opened) {
      openMenu();
    } else {
      closeMenu();
    }
  }




  // *******************************************
  // Открытие/закрытие подменю в мобильном меню
  // *******************************************
  
  // вешаем клик на родительские ссылки меню
  let menuParentLinks = menu.querySelectorAll('.menu__item--parent > .menu__link');
  menuParentLinks.forEach(link => link.addEventListener('click', parentLinkClickHandler));

  // клик на родительский пункт меню. При первом нажатии перехода по ссылке нет, открывается подменю, при повторном идет переход
  function parentLinkClickHandler(e) {
    if (dev) {
      console.log('parentLinkClickHandler');
    }

    // родительская ссылка, по которой нажали
    let link = e.target;

    if (link.wasClicked
        && link.getAttribute('href').search('#') !== -1) {
      return;
    } else {
      e.preventDefault();
      menuParentLinks.forEach(link => link.wasClicked = false);
      link.wasClicked = true;
    }

    let lastActiveSubMenu = menu.querySelector('.menu__subMenu--opened');
    if (lastActiveSubMenu) {
      lastActiveSubMenu.classList.remove('menu__subMenu--opened');
    }

    let subMenu = link.parentElement.getElementsByClassName('menu__subMenu')[0];
    subMenu.classList.add('menu__subMenu--opened');

  }


})();