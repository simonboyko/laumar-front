/* Подключаем слайдеры с помощью tiny-slider https://github.com/ganlanyuan/tiny-slider */

(function () {

  if (!document.querySelector('.itemSlider')) {
    return;
  }

  if (dev) {
    console.log('INIT sliders');
  }


  initItemSlider();
  initListingSlider();
  initVNZSlider();


  // слайдер на карточке объекта
  function initItemSlider() {

    let parentEl = document.getElementsByClassName('itemSlider--page')[0];

    if (!parentEl) {
      return;
    }

    if (dev) {
      console.log('f: initItemSlider');
    }


    // инициализируем слайдер
    let itemSlider = tns({
      container: parentEl.querySelector('.itemSlider__slides'),
      items: 1,
      controlsContainer: parentEl.querySelector('.itemSlider__arrows'),
      navContainer: parentEl.querySelector('.itemSlider__thumbnailsInner'),
      loop: true,
      mouseDrag: true
    });


    // двигаем превью при клике
    let thumbnails = parentEl.querySelectorAll('.itemSlider__thumbnail');
    let thumbnailsInner = parentEl.querySelector('.itemSlider__thumbnailsInner');
    let numOfThumbs = thumbnails.length;

    // устанавливаем ширину для контейнера
    thumbnailsInner.style.width = `calc(33.333% * ${numOfThumbs})`;

    // при изменении слайда переключаем точку и двигаем превью
    itemSlider.events.on('indexChanged', function () {
      moveThumbs();
      setActiveDot();
    });

    thumbnails.forEach(thumb => thumb.addEventListener('click', thumbClickHandler));

    function thumbClickHandler(e) {
      if (dev) {
        console.log('f: thumbClickHandler');
      }

      let index = e.target.getAttribute('data-nav');
      // moveThumbs(index);
      // setActiveDot(index)
    }

    // двигает превью, чтобы активный слайд был по середине
    function moveThumbs(index) {
      if (dev) {
        console.log('f: moveThumbs');
      }

      index = index || getCurrentIndex();

      let offset = (100 / numOfThumbs) * (index - 1);
      if (offset < 0) {
        thumbnailsInner.style.transform = 'translate3d(0,0,0)';
      } else {
        thumbnailsInner.style.transform = `translate3d(-${offset}%,0,0)`;
      }
    }


    // делаем точки кликабельными
    let dots = parentEl.querySelectorAll('.itemSlider__dot');
    dots.forEach((dot, index) => {
      // проставляем порядковые номера
      dot.setAttribute('data-nav', index);
      dot.addEventListener('click', dotClickHandler);
    });

    function dotClickHandler(e) {
      if (dev) {
        console.log('f: dotClickHandler');
      }

      let nextSlide = e.target.getAttribute('data-nav');
      itemSlider.goTo(nextSlide);
      // setActiveDot(nextSlide);
      // moveThumbs(nextSlide);
    }

    setActiveDot();

    // устанавливаем активную точку
    function setActiveDot(index) {
      if (dev) {
        console.log('f: setActiveDot');
      }

      index = index || getCurrentIndex();

      let currentActiveDot = parentEl.querySelector('.itemSlider__dot--active');
      if (currentActiveDot) {
        currentActiveDot.classList.remove('itemSlider__dot--active');
      }

      let nextActiveDot = parentEl.querySelector(`.itemSlider__dot[data-nav="${index}"]`);
      nextActiveDot.classList.add('itemSlider__dot--active');
    }


    // текущий индекс
    function getCurrentIndex() {
      return itemSlider.getInfo().displayIndex - 1;
    }

  }

  // слайдер в листинге
  function initListingSlider() {

    if (!document.querySelector('.itemsList')
    && !document.querySelector('.servicePopular__list')) {
      return;
    }

    if (dev) {
      console.log('f: initListingSlider');
    }

    let itemSlider = [];

    // все блоки со слайдерами
    let items = document.getElementsByClassName('itemSlider--listing');
    for (let i = 0; i < items.length; i++) {
      let item = items[i];
      itemSlider.push(tns({
        container: item.querySelector('.itemSlider__slides'),
        items: 1,
        controlsContainer: item.querySelector('.itemSlider__arrows'),
        navContainer: item.querySelector('.itemSlider__dots'),
        loop: false
      }));
    }

  }

  // слайдер на странице ВНЖ (2 карточки только на мобиле)
  function initVNZSlider() {
    let sliderWrapper = document.querySelector('.vnzTypes__slider');

    if (!sliderWrapper) return;

    if (dev) {
      console.log('f: initVNZSlider');
    }


    let s = tns({
      container: sliderWrapper.querySelector('.vnzTypes__list'),
      items: 1,
      controls: false,
      navContainer: sliderWrapper.querySelector('.vnzTypes__dots'),
      loop: false,
      responsive: {
        768: {
          disable: true
        }
      }
    });
  }

})();