(function (d, w) {
  let titer = d.querySelector('.titer--animated');

  if (!titer) return;

  if (dev) {
    console.log('INIT animation');
  }

  let titerH = titer.offsetHeight;

  let cloud1 = d.querySelector('.titer__cloud--1');
  let cloud2 = d.querySelector('.titer__cloud--2');
  let cloud3 = d.querySelector('.titer__cloud--3');
  let cloud4 = d.querySelector('.titer__cloud--4');
  let header = d.querySelector('.titer__heading');
  let img = d.querySelector('.titer__bg');

  w.addEventListener('load', ()=> {
    titer.classList.add('titer--animate');
    setTimeout(() => {
      w.addEventListener('scroll', scrollHandler);
    }, 3200);
  });
  
  function scrollHandler(e) {
    changeElsStyle();
    parallax();
  }
  

  function parallax() {
    if (w.scrollY > titerH) {
      return;
    }

    if (dev) {
      console.log('f: parallax');
    }

    cloud1.style.transform = `translate3D(0, ${w.scrollY*0.3}px, 0)`;
    cloud2.style.transform = `translate3D(0, ${w.scrollY*0.4}px, 0)`;
    cloud3.style.transform = `translate3D(0, ${w.scrollY*0.2}px, 0)`;
    cloud4.style.transform = `translate3D(0, ${w.scrollY*0.3}px, 0)`;
    header.style.transform = `translate3D(0, ${w.scrollY*0.2}px, 0)`;
    img.style.transform = `translate3D(0, ${w.scrollY*0.2}px, 0)`;
  }

  function changeElsStyle() {
    if (dev) {
      console.log('f: changeElsStyle');
    }
    d.querySelectorAll('.titer__cloud, .titer__heading, .titer__bg')
      .forEach((cloud) => {
        cloud.style.transitionDelay = '0s';
        cloud.style.transitionDuration = '0.1s';
      });
  }


})(document, window);