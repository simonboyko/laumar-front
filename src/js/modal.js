/* Скрипт для модальных окон. Чтобы работало, надо для элемента, при клике на который будет появляться модальное окно, добавить атрибут data-modal=".modal--question". Значение атрибута должно быть селектором нужного модального окна. */


(function () {
  if (dev) {
    console.log('INIT modal');
  }

  // выходим, если нет ссылок на модальные окна
  if (!document.querySelector('[data-modal]')) return;


  // ссылки на модальные окна, в атрибуте должен быть селектор на само модальное окно
  let links = document.querySelectorAll('[data-modal]');

  // навешиваем обработчик на клик
  links.forEach(link => link.addEventListener('click', clickHandler));


  // нажатие на любой крестик в модальном окне закрывает его
  let closers = document.querySelectorAll('.modal__close');
  closers.forEach(closer => closer.addEventListener('click', closeModal));

  // нажатие на фон в любом модальном окне закрывает его
  let modalCovers = document.querySelectorAll('.modal__bg');
  modalCovers.forEach(cover => cover.addEventListener('click', closeModal));

  // закрываем по ESC
  document.addEventListener('keyup', function (e) {
    if (openedModal && e.keyCode === 27) closeModal();
  });



  // сюда будем записывать текущее открытое модальное окно
  let openedModal;

  // обработчик на клик кнопки
  function clickHandler(e) {
    if (dev) {
      console.log('f: clickHandler');
    }
    e.preventDefault();

    // кнопка, по которой нажали
    let link = e.currentTarget;

    // берем нужное модальное окно
    openedModal = document.querySelector(link.getAttribute('data-modal'));


    // проверяем, есть ли такое модальное окно
    if (openedModal) {
      // показываем его
      openModal();
    } else {
      console.error('Такого модального окна нет! Проверьте селектор в атрибуте data-modal.');
    }

  }

  // вызываем, чтобы показать модалку
  function openModal() {
    if (dev) {
      console.log('f: openModal');
    }

    openedModal.classList.add('modal--opened');

    let scrollWidth = getScrollWidth();
    if (scrollWidth > 0) {
      document.body.style.marginRight = scrollWidth + 'px';
    }

    document.body.classList.add('noScroll');

  }

  // вызываем, чтобы скрыть модалку
  function closeModal() {
    if (dev) {
      console.log('f: closeModal');
    }

    openedModal.classList.remove('modal--opened');
    document.body.classList.remove('noScroll');
    document.body.style.marginRight = '';

    // стираем инфо о текущем открытом окне
    openedModal = null;
  }
  
  // ширина скролла
  function getScrollWidth() {
    if (dev) {
      console.log('f: getScrollWidth');
    }
    let scrollWidth = window.innerWidth - document.body.clientWidth;

    console.log(scrollWidth);

    return scrollWidth;
  }



})();