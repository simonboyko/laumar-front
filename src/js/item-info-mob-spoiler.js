/* На мобильных ширинах должен появляться спойлер для текста на в блоке "Описание" на странице карточки объекта */

(function () {
  if (!(document.getElementsByClassName('itemDescription__body')[0]
    && document.body.clientWidth < 768)) return;

  if (dev) {
    console.log('INIT item-info-mob-spoiler');
  }


  // элемент с текстом
  let text = document.getElementsByClassName('itemDescription__body')[0];

  // кол-во показываемых строк и высота строки
  const LINES = 4;
  let lineHeight = parseFloat(getComputedStyle(text).lineHeight);

  // определяем ширину открытого и закрытого блока
  let expandedHeight = text.offsetHeight + 'px';
  let collapsedHeight = LINES * lineHeight + 'px';


  // создаем кнопку
  let button = document.createElement('button');
  button.innerHTML = 'Читать дальше';
  button.className = 'itemDescription__spoiler';
  text.parentElement.appendChild(button);
  button.addEventListener('click', buttonClickHandler);
  function buttonClickHandler(e) {
    if (dev) {
      console.log('f: buttonClickHandler');
    }

    if (text.spoiler === 'expanded') {
      collapse();
    } else if (text.spoiler === 'collapsed') {
      expand();
    } else {
      console.log('Неизвестно состояние текста.');
    }
  }


  // по умолчанию скрываем
  collapse();


  // раскрывает текст
  function expand() {
    if (dev) {
      console.log('f: expand');
    }

    text.spoiler = 'expanded';
    text.style.height = expandedHeight;
    button.innerHTML = 'Скрыть текст';
  }

  // скрывает текст
  function collapse() {
    if (dev) {
      console.log('f: collapse');
    }

    text.spoiler = 'collapsed';
    text.style.height = collapsedHeight;
    button.innerHTML = 'Читать дальше';
  }

})();