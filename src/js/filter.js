(function (d) {
  if (!d.querySelector('.form--filter')) return;

  function debounce(f, ms) {

    let timer = null;

    return function (...args) {
      const onComplete = () => {
        f.apply(this, args);
        timer = null;
      };

      if (timer) {
        clearTimeout(timer);
      }

      timer = setTimeout(onComplete, ms);
    };
  }


  /**
   * Ajax поиск по городу
   */
  class CityField {
    constructor(field) { 
      this.url = '/wp-json/wp/v2/tags?search=';
      this.field = field;
      this.list;
      this.listItems;
      this.listOpened = false;
      this.currentFocusedItem = -1;
      this.createListWrapper();
      this.addEvents();
    }

    addEvents() {
      this.field.addEventListener('input', debounce((e) => {
        console.log(e.target.value);
        this.inputHandler(e.target.value);
      }, 300).bind(this));
      d.addEventListener('keydown', this.keydownHandler.bind(this));
    }

    addListItemEvents() {
      this.listItems.forEach(i => i.addEventListener('click', this.listClickHandler.bind(this)));
    }

    keydownHandler(e) {
      const KEY_DOWN = 40;
      const KEY_UP = 38;


      if (!this.listOpened) {
        return;
      }

      if (e.keyCode === KEY_DOWN) {
        e.preventDefault();
        this.selectNextItem();
      }

      if (e.keyCode === KEY_UP) {
        e.preventDefault();
        this.selectPrevItem();
      }
    }

    selectNextItem() {
      if (this.currentFocusedItem < this.listItems.length - 1
          && this.currentFocusedItem >= -1) {

        if (dev) {
          console.log('f: selectNextItem');
        }

        this.currentFocusedItem++;
        console.log(this.currentFocusedItem);
        this.listItems[this.currentFocusedItem].focus();

      }
    }

    selectPrevItem() {
      if (this.currentFocusedItem <= this.listItems.length - 1
        && this.currentFocusedItem > 0) {

        if (dev) {
          console.log('f: selectPrevItem');
        }

        this.currentFocusedItem--;
        console.log(this.currentFocusedItem);
        this.listItems[this.currentFocusedItem].focus();

      }
    }

    listClickHandler(e) {
      if (dev) {
        console.log('f: listClickHandler');
      }
      this.field.value = e.target.innerHTML;
      this.hideList();
    }

    inputHandler(str) {
      if (!str) {
        this.hideList();
        return;
      }
      this.getData(str)
        .then(res => {
          let arr = [];
          for (let i = 0; i < res.length; i++) {
            arr.push(res[i].name);
          }
          if (arr.length > 0) {
            this.showList(arr);
          } else {
            this.hideList();
          }
        })
        .catch(e => console.log(e));
    }

    getData(str) {
      return new Promise((res, rej) => {
        str = str || '';
        let xhr = new XMLHttpRequest();
        xhr.open('get', this.url + str, true);
        xhr.send();
        xhr.addEventListener('load', () => {
          res(JSON.parse(xhr.response));
        });
      });
    }

    createListWrapper() {
      this.list = document.createElement('div');
      this.list.className = 'cityList';
    }

    showList(arr) {
      let html = '';
      for (let i = 0; i < arr.length; i++) {
        html += `<button class="cityList__item">${arr[i]}</button>`;
      }
      this.list.innerHTML = html;
      this.field.parentElement.appendChild(this.list);
      this.listItems = this.list.querySelectorAll('.cityList__item');
      this.addListItemEvents();
      this.listOpened = true;
    }

    hideList() {
      this.list.remove();
      this.listOpened = false;
    }
  }

  /**
   * Переключение форм
   */
  class FormToggler {
    constructor(form) {
      this.form = form;
      this.value = '';
      this.state = '';
      this.selects = form.querySelectorAll('.form__field--type');
      this.formContent = {
        buy: form.querySelector('.form__main--buy'),
        rent: form.querySelector('.form__main--rent')
      };
      form.querySelector('.form__main[hidden]').removeAttribute('hidden');
      this.initStateAndValue();
      this.addEvents();
      this.updateForm();
    }

    initStateAndValue() {
      if (dev) {
        console.log('f: getStateAtFirst');
      }
      let select = this.selects[0];
      let selectedOption = select.options[select.selectedIndex];
      this.value = selectedOption.value;
      this.state = selectedOption.getAttribute('data-form-type');
    }

    changeState(e) {
      if (dev) {
        console.log('f: changeState');
      }
      let select = e.target;
      let opt = select.options[select.selectedIndex];
      this.state = opt.getAttribute('data-form-type');
      this.value = opt.value;
      this.updateForm();
      this.updateSelects();
    }

    updateForm() {
      if (dev) {
        console.log('f: updateForm');
      }
      this.form.innerHTML = '';
      this.form.appendChild(this.formContent[this.state]);

      if (this.state === 'rent') {
        this.form.classList.add('form--rent');
        this.form.classList.remove('form--buy');

      } else {
        this.form.classList.add('form--buy');
        this.form.classList.remove('form--rent');
      }
    }

    updateSelects() {
      if (dev) {
        console.log('f: updateSelects');
      }
      this.selects.forEach(select => {
        select.value = this.value;
      });
    }

    addEvents() {
      if (dev) {
        console.log('f: addEvents');
      }
      this.selects
        .forEach(
          i => i.addEventListener('change', this.changeState.bind(this))
        );
    }

  }


  /**
   * Функционал ajax-фильтра
   */
  class Filter {
    constructor(form, content, pager) {
      if (window.apiSite && window.apiSite.urlRestCatalog) {
        this.url = window.apiSite.urlRestCatalog;
      } else {
        this.url = 'http://localhost:3000';
      }
      this.form = document.querySelector(form);
      this.content = document.querySelector(content);
      this.pager = document.querySelector(pager);
      new FormToggler(this.form);
      this.initCityField();

      // this.addEvents();
    }

    sendForm(data) {
      if (dev) {
        console.log('f: getData');
      }

      let xhr = new XMLHttpRequest();
      xhr.open('post', this.url);
      xhr.send(this.getStringifyFormData());
      if (xhr.status == 200) {
        this.data = JSON.parse(xhr.responseText);
      }

      this.data = this.getTestData();
      this.updateView();

    }

    getTestData() {
      return {
        items: [
          '<li class="itemsList__item">\n' +
          '              <!-- itemCardInList-->\n' +
          '              <article class="itemCardInList">\n' +
          '                <div class="itemCardInList__slider">\n' +
          '                  <!-- itemListingSlider-->\n' +
          '                  <div class="itemSlider itemSlider--listing">\n' +
          '                    <div class="itemSlider__main">\n' +
          '                      <div class="tns-outer" id="tns1-ow"><div class="tns-liveregion tns-visually-hidden" aria-live="polite" aria-atomic="true">slide <span class="current">1</span>  of 3</div><div id="tns1-mw" class="tns-ovh"><div class="tns-inner" id="tns1-iw"><ul class="itemSlider__slides  tns-slider tns-carousel tns-subpixel tns-calc tns-horizontal" id="tns1" style="transition-duration: 0s; transform: translate3d(0%, 0px, 0px);">\n' +
          '                        <li class="itemSlider__slide tns-item tns-slide-active" id="tns1-item0">\n' +
          '                          <picture>\n' +
          '                            <source srcset="./files/villa-listing@mobile.jpg" media="(max-width: 767px)"><img src="./files/villa-listing.jpg" alt=""></picture>\n' +
          '                        </li>\n' +
          '                        <li class="itemSlider__slide tns-item" id="tns1-item1" aria-hidden="true" tabindex="-1">\n' +
          '                          <picture>\n' +
          '                            <source srcset="./files/villa-listing@mobile.jpg" media="(max-width: 767px)"><img src="./files/villa-listing.jpg" alt=""></picture>\n' +
          '                        </li>\n' +
          '                        <li class="itemSlider__slide tns-item" id="tns1-item2" aria-hidden="true" tabindex="-1">\n' +
          '                          <picture>\n' +
          '                            <source srcset="./files/villa-listing@mobile.jpg" media="(max-width: 767px)"><img src="./files/villa-listing.jpg" alt=""></picture>\n' +
          '                        </li>\n' +
          '                      </ul></div></div></div>\n' +
          '                      <div class="itemSlider__dots" aria-label="Carousel Pagination"><button class="itemSlider__dot tns-nav-active" data-nav="0" aria-label="Carousel Page 1 (Current Slide)" aria-controls="tns1">1</button><button class="itemSlider__dot" data-nav="1" tabindex="-1" aria-label="Carousel Page 2" aria-controls="tns1">2</button><button class="itemSlider__dot" data-nav="2" tabindex="-1" aria-label="Carousel Page 3" aria-controls="tns1">3</button></div>\n' +
          '                      <div class="itemSlider__arrows" aria-label="Carousel Navigation" tabindex="0"><button class="itemSlider__arrow itemSlider__arrow--prev" aria-controls="tns1" tabindex="-1" data-controls="prev" disabled="">\n' +
          '                          &lt;</button> <button class="itemSlider__arrow itemSlider__arrow--next" aria-controls="tns1" tabindex="-1" data-controls="next">&gt;\n' +
          '                        </button></div>\n' +
          '                    </div>\n' +
          '                  </div>\n' +
          '                </div>\n' +
          '                <div class="itemCardInList__main">\n' +
          '                  <div class="itemCardInList__id">469245</div>\n' +
          '                  <h3 class="itemCardInList__title"><a href="item-sale.html">Продажа коттеджей в Валенсии, Испания</a></h3>\n' +
          '                  <div class="itemCardInList__name">Escultores de la Rivera. Riviera del de Sol-Miraflores</div>\n' +
          '                  <div class="itemCardInList__stats">\n' +
          '                    <!-- itemStats-->\n' +
          '                    <div class="itemStats itemStats--listing">\n' +
          '                      <div class="itemStats__stat">\n' +
          '                        <div class="itemStats__value itemStats__value--territory">18 м²</div>\n' +
          '                        <div class="itemStats__label">Территория</div>\n' +
          '                      </div>\n' +
          '                      <div class="itemStats__stat itemStats__stat--important">\n' +
          '                        <div class="itemStats__value itemStats__value--area">268 м²</div>\n' +
          '                        <div class="itemStats__label">Площадь</div>\n' +
          '                      </div>\n' +
          '                      <div class="itemStats__stat">\n' +
          '                        <div class="itemStats__value itemStats__value--bedrooms">4</div>\n' +
          '                        <div class="itemStats__label">Спальни</div>\n' +
          '                      </div>\n' +
          '                      <div class="itemStats__stat">\n' +
          '                        <div class="itemStats__value itemStats__value--bathrooms">2</div>\n' +
          '                        <div class="itemStats__label">Ванные</div>\n' +
          '                      </div>\n' +
          '                      <!--+e(\'stat\')\n' +
          '  +e(\'value\', \'newBuilding\')\n' +
          '  +e(\'label\') Новостройка\n' +
          '+e(\'stat\')\n' +
          '  +e(\'value\', \'sleepPlace\') 7\n' +
          '  +e(\'label\') Спальных мест\n' +
          '+e(\'stat\')\n' +
          '  +e(\'value\', \'beach\') 1,8 км\n' +
          '  +e(\'label\') До пляжа-->\n' +
          '                    </div>\n' +
          '                  </div>\n' +
          '                  <div class="itemCardInList__footer">\n' +
          '                    <div class="itemCardInList__price">от 160 €/сутки</div><a class="itemCardInList__link" href="item-sale.html">Подробнее <svg width="5" height="8" viewBox="0 0 5 8" xmlns="http://www.w3.org/2000/svg">\n' +
          '                        <path d="M4.35355 4.35355C4.54882 4.15829 4.54882 3.84171 4.35355 3.64645L1.17157 0.464466C0.976311 0.269204 0.659728 0.269204 0.464466 0.464466C0.269204 0.659728 0.269204 0.976311 0.464466 1.17157L3.29289 4L0.464466 6.82843C0.269204 7.02369 0.269204 7.34027 0.464466 7.53553C0.659728 7.7308 0.976311 7.7308 1.17157 7.53553L4.35355 4.35355ZM3 4.5H4V3.5H3V4.5Z"></path>\n' +
          '                      </svg>\n' +
          '                    </a>\n' +
          '                  </div>\n' +
          '                </div>\n' +
          '              </article>\n' +
          '            </li>',
          '<li class="itemsList__item">\n' +
          '              <!-- itemCardInList-->\n' +
          '              <article class="itemCardInList">\n' +
          '                <div class="itemCardInList__slider">\n' +
          '                  <!-- itemListingSlider-->\n' +
          '                  <div class="itemSlider itemSlider--listing">\n' +
          '                    <div class="itemSlider__main">\n' +
          '                      <div class="tns-outer" id="tns1-ow"><div class="tns-liveregion tns-visually-hidden" aria-live="polite" aria-atomic="true">slide <span class="current">1</span>  of 3</div><div id="tns1-mw" class="tns-ovh"><div class="tns-inner" id="tns1-iw"><ul class="itemSlider__slides  tns-slider tns-carousel tns-subpixel tns-calc tns-horizontal" id="tns1" style="transition-duration: 0s; transform: translate3d(0%, 0px, 0px);">\n' +
          '                        <li class="itemSlider__slide tns-item tns-slide-active" id="tns1-item0">\n' +
          '                          <picture>\n' +
          '                            <source srcset="./files/villa-listing@mobile.jpg" media="(max-width: 767px)"><img src="./files/villa-listing.jpg" alt=""></picture>\n' +
          '                        </li>\n' +
          '                        <li class="itemSlider__slide tns-item" id="tns1-item1" aria-hidden="true" tabindex="-1">\n' +
          '                          <picture>\n' +
          '                            <source srcset="./files/villa-listing@mobile.jpg" media="(max-width: 767px)"><img src="./files/villa-listing.jpg" alt=""></picture>\n' +
          '                        </li>\n' +
          '                        <li class="itemSlider__slide tns-item" id="tns1-item2" aria-hidden="true" tabindex="-1">\n' +
          '                          <picture>\n' +
          '                            <source srcset="./files/villa-listing@mobile.jpg" media="(max-width: 767px)"><img src="./files/villa-listing.jpg" alt=""></picture>\n' +
          '                        </li>\n' +
          '                      </ul></div></div></div>\n' +
          '                      <div class="itemSlider__dots" aria-label="Carousel Pagination"><button class="itemSlider__dot tns-nav-active" data-nav="0" aria-label="Carousel Page 1 (Current Slide)" aria-controls="tns1">1</button><button class="itemSlider__dot" data-nav="1" tabindex="-1" aria-label="Carousel Page 2" aria-controls="tns1">2</button><button class="itemSlider__dot" data-nav="2" tabindex="-1" aria-label="Carousel Page 3" aria-controls="tns1">3</button></div>\n' +
          '                      <div class="itemSlider__arrows" aria-label="Carousel Navigation" tabindex="0"><button class="itemSlider__arrow itemSlider__arrow--prev" aria-controls="tns1" tabindex="-1" data-controls="prev" disabled="">\n' +
          '                          &lt;</button> <button class="itemSlider__arrow itemSlider__arrow--next" aria-controls="tns1" tabindex="-1" data-controls="next">&gt;\n' +
          '                        </button></div>\n' +
          '                    </div>\n' +
          '                  </div>\n' +
          '                </div>\n' +
          '                <div class="itemCardInList__main">\n' +
          '                  <div class="itemCardInList__id">469245</div>\n' +
          '                  <h3 class="itemCardInList__title"><a href="item-sale.html">Продажа коттеджей в Валенсии, Испания</a></h3>\n' +
          '                  <div class="itemCardInList__name">Escultores de la Rivera. Riviera del de Sol-Miraflores</div>\n' +
          '                  <div class="itemCardInList__stats">\n' +
          '                    <!-- itemStats-->\n' +
          '                    <div class="itemStats itemStats--listing">\n' +
          '                      <div class="itemStats__stat">\n' +
          '                        <div class="itemStats__value itemStats__value--territory">18 м²</div>\n' +
          '                        <div class="itemStats__label">Территория</div>\n' +
          '                      </div>\n' +
          '                      <div class="itemStats__stat itemStats__stat--important">\n' +
          '                        <div class="itemStats__value itemStats__value--area">268 м²</div>\n' +
          '                        <div class="itemStats__label">Площадь</div>\n' +
          '                      </div>\n' +
          '                      <div class="itemStats__stat">\n' +
          '                        <div class="itemStats__value itemStats__value--bedrooms">4</div>\n' +
          '                        <div class="itemStats__label">Спальни</div>\n' +
          '                      </div>\n' +
          '                      <div class="itemStats__stat">\n' +
          '                        <div class="itemStats__value itemStats__value--bathrooms">2</div>\n' +
          '                        <div class="itemStats__label">Ванные</div>\n' +
          '                      </div>\n' +
          '                      <!--+e(\'stat\')\n' +
          '  +e(\'value\', \'newBuilding\')\n' +
          '  +e(\'label\') Новостройка\n' +
          '+e(\'stat\')\n' +
          '  +e(\'value\', \'sleepPlace\') 7\n' +
          '  +e(\'label\') Спальных мест\n' +
          '+e(\'stat\')\n' +
          '  +e(\'value\', \'beach\') 1,8 км\n' +
          '  +e(\'label\') До пляжа-->\n' +
          '                    </div>\n' +
          '                  </div>\n' +
          '                  <div class="itemCardInList__footer">\n' +
          '                    <div class="itemCardInList__price">от 160 €/сутки</div><a class="itemCardInList__link" href="item-sale.html">Подробнее <svg width="5" height="8" viewBox="0 0 5 8" xmlns="http://www.w3.org/2000/svg">\n' +
          '                        <path d="M4.35355 4.35355C4.54882 4.15829 4.54882 3.84171 4.35355 3.64645L1.17157 0.464466C0.976311 0.269204 0.659728 0.269204 0.464466 0.464466C0.269204 0.659728 0.269204 0.976311 0.464466 1.17157L3.29289 4L0.464466 6.82843C0.269204 7.02369 0.269204 7.34027 0.464466 7.53553C0.659728 7.7308 0.976311 7.7308 1.17157 7.53553L4.35355 4.35355ZM3 4.5H4V3.5H3V4.5Z"></path>\n' +
          '                      </svg>\n' +
          '                    </a>\n' +
          '                  </div>\n' +
          '                </div>\n' +
          '              </article>\n' +
          '            </li>'
        ]
      }
    }
    
    getStringifyFormData() {
      let data = {};
      let FD = new FormData(this.form);

      FD.forEach((value, key) => {
        data[key] = value;
      });

      return JSON.stringify(data);
    }

    addEvents() {
      this.form.addEventListener('submit', this.submitHandler.bind(this));
    }
    
    submitHandler(e) {
      e.preventDefault();
      this.sendForm()
    }

    updateView() {
      if (dev) {
        console.log('f: updateView');
      }
      this.renderContent();
    }

    renderContent() {
      if (dev) {
        console.log('f: renderContent');
      }
      let html = '';
      this.data.items.forEach(item => {
        html += item;
      });

      this.content.innerHTML = html;
    }

    initCityField() {
      this.form.querySelectorAll('.form__field--city')
        .forEach(field => new CityField(field));
    }
  }

  try {
    new Filter('.form--filter', '.itemsList__list', '.itemsList__pager');
  } catch(e) {
    console.log(e);
  }
})(document);