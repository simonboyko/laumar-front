/* Спойлер в карточке характеристик на странице карточки объекта */

(function () {
  let spoiler = document.getElementsByClassName('itemInfo__spoiler')[0];
  if (!spoiler) return;

  if (dev) {
    console.log('INIT spoiler');
  }

  const LIMIT = 6;

  // если элементов мало, скрываем кнопку
  let items = spoiler.parentElement.querySelectorAll('.itemInfo__item');
  if (items.length <= LIMIT) {
    spoiler.remove();
  }

  // устанавливаем начальную высоту для списка
  let list = spoiler.parentElement.querySelector('.itemInfo__list');

  let expandedHeight;
  let limitEl;
  let collapsedHeight;

  // иначе высота определяется неправильно
  window.addEventListener('load', function () {
    expandedHeight = list.offsetHeight;
    limitEl = list.getElementsByClassName('itemInfo__item')[LIMIT - 1];
    collapsedHeight = limitEl.offsetTop + limitEl.offsetHeight;
    collapseList();
  });



  // клик на спойлер
  spoiler.addEventListener('click', spoilerClickHandler);
  function spoilerClickHandler(e) {
    if (dev) {
      console.log('f: spoilerClickHandler');
    }

    if (list.spoiler === 'collapsed') {
      expandList();
    } else if (list.spoiler === 'expanded') {
      collapseList();
    } else {
      console.error('Спойлер: что-то пошло не так. Немогу определить состояние списка.');
    }
  }

  // сворачиваем
  function collapseList() {
    list.style.height = collapsedHeight + 'px';
    list.spoiler =  'collapsed';
    spoiler.classList.remove('itemInfo__spoiler--opened');
  }

  // разворачиваем
  function expandList() {
    list.style.height = expandedHeight + 'px';
    list.spoiler =  'expanded';
    spoiler.classList.add('itemInfo__spoiler--opened');
  }
})();